<?php

use ClassList\Service\CompetenceService;
use ClassList\Service\ExperienceService;
use ClassList\Service\TwigClass;
use ClassList\Service\TypeCompetenceService;

require_once 'vendor/autoload.php';
$route = $_GET["link"] ?? "";
$_SESSION["user"] = "admin";
const ADMIN = "admin";
const INDEX = 'index.twig';
$template = INDEX;

switch ($route) {
    case "experience" :
        $template = 'experience.twig';
        $serviceUsed = new ExperienceService();
        $dataUsed = $serviceUsed->getReturn();
        break;
    case "competence":
        $template = 'competence.twig';
        $serviceUsed = new CompetenceService();
        $dataUsed = $serviceUsed->getReturn();
        break;
    case "admin":
        if ($_SESSION["user"] == ADMIN) {
            $subRoute = $_GET["sublink"] ?? "";
            switch ($subRoute) {
                case "exp":
                    $template = 'experienceAdmin.twig';
                    $serviceUsed = new ExperienceService();
                    $dataUsed = $serviceUsed->getReturn();
                    break;
                case "comp":
                    $template = 'competenceAdmin.twig';
                    $serviceUsed = new CompetenceService();
                    $dataUsed = $serviceUsed->getReturn();
                    break;
                case "type":
                    $template = 'typeAdmin.twig';
                    $serviceUsed = new TypeCompetenceService();
                    if(isset($_POST["code"])) {
                        $dataFromPost = $serviceUsed->takeDataFromPost();
                        $serviceUsed->administration(
                            mode: $_SESSION["user"],
                            code: $dataFromPost["code"],
                            id: $dataFromPost["id"],
                            name: $dataFromPost["name"],
                            description: $dataFromPost["description"]
                        );
                    }
                    $idByGet = $_GET["id"] ?? null;
                    $dataUsed = $serviceUsed->getData(intval($idByGet));
                    break;
                default:
                    $template = INDEX;
            }
        }
        break;
    default:
        $template = INDEX;
        break;
}
/*
 * Affichage de la route
 */
$data = $dataUsed ?? [];
$twigClass = new TwigClass();
$twig = $twigClass->getTwig();
$data += ["mode" => $_SESSION["user"]];
try {
    echo $twig->load($template)->render($data);
} catch (exception $exception) {
    echo $exception->getMessage();
}
