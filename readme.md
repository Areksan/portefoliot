# Cadre

Réalisation d'un Portefolio maison:

stack : PHP8.0 + Twig + SqLite

# Lancement

- Ajouter le projet dans votre environnement serveur;
- Créer une base de donnée sqLite nommer : portfoliot.sqlite;
- Utiliser dbSource.sql pour alimenter la bdd portfoliot.
- Utiliser composer install pour installer Twig.
