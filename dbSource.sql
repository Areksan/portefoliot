PRAGMA foreign_keys = ON;

create table TypeCompetence
(
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    name        VARCHAR(255),
    description TEXT
);

create table Experience
(
    id             INTEGER PRIMARY KEY AUTOINCREMENT,
    name           VARCHAR(255),
    nameEntreprise VARCHAR(255),
    start          DATE,
    end            DATE,
    description    TEXT
);

create table Competence
(
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    name        VARCHAR(255),
    description TEXT
);

create table User
(
    id       INTEGER PRIMARY KEY AUTOINCREMENT,
    name     VARCHAR(255),
    password VARCHAR(512)
);

create table Competence_TypeCompetence
(
    id                INTEGER PRIMARY KEY AUTOINCREMENT,
    competence_id     integer,
    typeCompetence_id integer,
    FOREIGN KEY (competence_id) REFERENCES Competence (id),
    FOREIGN KEY (typeCompetence_id) REFERENCES TypeCompetence (id)
);

create table Competence_Experience
(
    id            INTEGER PRIMARY KEY AUTOINCREMENT,
    competence_id integer,
    experience_id integer,
    FOREIGN KEY (competence_id) REFERENCES Competence (id),
    FOREIGN KEY (experience_id) REFERENCES Experience (id)
);
