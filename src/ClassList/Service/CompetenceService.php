<?php


namespace ClassList\Service;


use ClassList\DAO\CompetenceDAO;
use ClassList\DataClass\Competence;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

class CompetenceService
{
    private CompetenceDAO $compDao;

    #[Pure] public function __construct()
    {
        $this->compDao = new CompetenceDAO();
    }

    #[ArrayShape(["competences" => "array"])] private function getCompetences(): array
    {
        $id = $_GET["id"] ?? "";
        if ($id) {
            $dataUsed = $this->compDao->getData($id);
        } else {
            $dataUsed = $this->compDao->getData();
        }
        return ["competences" => $dataUsed];
    }

    #[ArrayShape(["competences" => "array"])] private function createCompetence(): array
    {
        $comp = new Competence();
        $comp->setName($_POST["name"]);
        $comp->setDescription($_POST["description"]);
        $this->compDao->createData($comp);
        return ["competences" => $this->compDao->getData()];
    }

    #[ArrayShape(["Competences" => "array"])] private function deleteCompetence(): array
    {
        $compToDelete = $this->compDao->getData(intval($_POST["delId"]));
        $this->compDao->deleteData($compToDelete[0]);
        return ["competences" => $this->compDao->getData()];
    }

    #[ArrayShape(["competences" => "array"])] private function editCompetence(): array
    {
        $comp = new Competence();
        $comp->setId($_POST["id"]);
        $comp->setName($_POST["name"]);
        $comp->setDescription($_POST["description"]);
        $this->compDao->updateData($comp);
        return ["competences" => $this->compDao->getData()];
    }

    function getReturn(): array
    {
        if ($_SESSION["mode"] == "admin") {
            $postReturn = $_POST["code"] ?? "";
            return match ($postReturn) {
                "delId" => $this->deleteCompetence(),
                "create" => $this->createCompetence(),
                "edit" => $this->editCompetence(),
                default => $this->getCompetences(),
            };
        } else {
            return $this->getCompetences();
        }
    }
}
