<?php


namespace ClassList\Service;


use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class TwigClass
{
    private Environment $twig;

    public function __construct()
    {
        $dirToTemplates = realpath('src/templates');
        $templateTab = [$dirToTemplates, $dirToTemplates . '/element',$dirToTemplates . '/admin'];
        $loader = new FilesystemLoader($templateTab);
        $this->twig = new Environment($loader);
    }

    public function getTwig(): Environment
    {
        return $this->twig;
    }
}
