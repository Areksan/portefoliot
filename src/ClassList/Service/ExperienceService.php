<?php


namespace ClassList\Service;


use ClassList\DAO\ExperienceDAO;
use ClassList\DataClass\Experience;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

class ExperienceService
{
    private ExperienceDAO $expDao;

    #[Pure] public function __construct()
    {
        $this->expDao = new ExperienceDAO();
    }

    #[ArrayShape(['experiences' => "array"])] private function getExperiences(): array
    {
        $id = $_GET["id"] ?? false;
        if ($id) {
            $dataUsed = ['experiences' => $this->expDao->getData(intval($_GET["id"]))];
        } else {
            $dataUsed = ['experiences' => $this->expDao->getData()];
        }
        return $dataUsed;
    }

    #[ArrayShape(['experiences' => "array"])] private function createExperience(): array
    {
        $exp = new Experience();
        $exp->setName($_POST["name"]);
        $exp->setNameEntreprise($_POST["nameEntreprise"]);
        $exp->setStart($_POST["start"]);
        $exp->setEnd($_POST["end"]);
        $exp->setDescription($_POST["description"]);
        $this->expDao->createData($exp);
        return ['experiences' => $this->expDao->getData()];
    }

    #[ArrayShape(['experiences' => "array"])] private function deleteExperience(): array
    {
        $expToDelete = $this->expDao->getData(intval($_POST["delId"]));
        $this->expDao->deleteData($expToDelete[0]);
        return ['experiences' => $this->expDao->getData()];
    }

    #[ArrayShape(['experiences' => "array"])] private function editExperience(): array
    {
        $exp = new Experience();
        $exp->setId($_POST["id"]);
        $exp->setName($_POST["name"]);
        $exp->setNameEntreprise($_POST["nameEntreprise"]);
        $exp->setStart($_POST["start"]);
        $exp->setEnd($_POST["end"]);
        $exp->setDescription($_POST["description"]);
        $this->expDao->updateData($exp);
        return ['experiences' => $this->expDao->getData()];
    }

    function getReturn(): array
    {
        if ($_SESSION["mode"] == "admin") {
            $postReturn = $_POST["code"] ?? "";
            return match ($postReturn) {
                "delId" => $this->deleteExperience(),
                "create" => $this->createExperience(),
                "edit" => $this->editExperience(),
                default => $this->getExperiences(),
            };
        } else {
            return $this->getExperiences();
        }
    }
}
