<?php


namespace ClassList\Service;


use ClassList\DAO\TypeCompetenceDAO;
use ClassList\DataClass\TypeCompetence;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

class TypeCompetenceService
{
    private TypeCompetenceDAO $typeDao;
    const ADMIN = "admin";

    #[Pure] public function __construct()
    {
        $this->typeDao = new TypeCompetenceDAO();
    }

    #[ArrayShape(["typeCompetences" => "array"])]
    private function getAll(): array
    {
        return ["typeCompetences" => $this->typeDao->getData()];
    }

    #[ArrayShape(["TypeCompetences" => "array"])]
    private function getById($id): array
    {
        return ["typeCompetences" => $this->typeDao->getData($id)];
    }

    /**
     * @param string $name
     * @param string $description
     * @return null
     */
    private function create(string $name, string $description)
    {
        $type = new TypeCompetence();
        $type->setName($name);
        $type->setDescription($description);
        $this->typeDao->createData($type);
        return null;
    }

    /**
     * @param int $id
     * @return bool
     */
    private function delete(int $id): bool
    {
        $typeToDelete = $this->typeDao->getData($id);
        return $this->typeDao->deleteData($typeToDelete[0]);
    }

    /**
     * @param int $id
     * @param string $name
     * @param string $description
     * @return null
     */
    private function edit(int $id, string $name, string $description)
    {
        $type = new TypeCompetence();
        $type->setId($id);
        $type->setName($name);
        $type->setDescription($description);
        $this->typeDao->updateData($type);
        return null;
    }

    /**
     * @param string $mode
     * @param string $code
     * @param int|null $id
     * @param string|null $name
     * @param string|null $description
     * @return bool
     */
    public function administration(string $mode, string $code, int|null $id, string|null $name, string|null $description): bool
    {
        if ($mode == self::ADMIN) {
            switch ($code) {
                case "delId":
                    $this->delete($id);
                    break;
                case "create":
                    $this->create($name, $description);
                    break;
                case "edit":
                    $this->edit($id, $name, $description);
                    break;
                default:
                    return false;
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * @param int|null $id
     * @return array
     */
    public function getData(int|null $id = null): array
    {
        return $id != null ? $this->getById($id) : $this->getAll();
    }

    #[ArrayShape(["code" => "mixed|string", "id" => "int|null", "name" => "mixed|null", "description" => "mixed|null"])]
    public function takeDataFromPost(): array
    {
        $code = $_POST["code"] ?? "";
        $id = $_POST["id"] ?? null;
        $name = $_POST["name"] ?? null;
        $description = $_POST["description"] ?? null;

        return [
            "code" => $code,
            "id" => intval($id),
            "name" => $name,
            "description" => $description
        ];
    }
}
