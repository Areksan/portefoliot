<?php


namespace ClassList\Service;


use DateInterval;
use Exception;
use PDO;

class DbConnexion
{
    private static $cnx;
    private static $instance = null;
    private $host, $db, $user, $pass, $charset, $dsn, $options, $limdate;

    private function __construct()
    {
        $this->host = '127.0.0.1';
        $this->db = 'portfoliot';
        $this->user = 'root';
        $this->pass = '';
        $this->charset = 'utf8';
        $this->dsn = "sqlite:$this->db.sqlite";
        $this->options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];
        $this->limdate = new DateInterval('P3W');
        try {
            self::$cnx = new PDO($this->dsn, $this->user, $this->pass, $this->options);
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    public static function getConnexion()
    {
        if (!isset(self::$instance)) {
            self::$instance = new DbConnexion();
        }
        return self::$instance;
    }

    function getCnx()
    {
        return self::$cnx;
    }
}
