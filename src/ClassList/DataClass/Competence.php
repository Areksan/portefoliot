<?php


namespace ClassList\DataClass;


use ArrayObject;

class Competence
{
    private int $id;
    private string $name;
    private ArrayObject $typeCompetences;
    private string $description;

    public function __construct()
    {
        $this->typeCompetences = new ArrayObject();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @param string $name
     * @return Competence
     */
    public function setName(string $name): Competence
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param TypeCompetence $typeCompetence
     * @return Competence
     */
    public function addTypeCompetence(TypeCompetence $typeCompetence): Competence
    {
        $this->typeCompetences->append($typeCompetence);
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return ArrayObject
     */
    public function getTypeCompetences(): ArrayObject
    {
        return $this->typeCompetences;
    }

    public function deleteTypeCompetence(TypeCompetence $delCompetence): Competence
    {
        /** @var TypeCompetence $typeCompetence */
        foreach ($this->typeCompetences as $key => $typeCompetence) {
            if ($this->typeCompetences->offsetGet($key) === $delCompetence) {
                $this->typeCompetences->offsetUnset($key);
                break;
            }
        }
        return $this;
    }

    /**
     * @param string $description
     * @return Competence
     */
    public function setDescription(string $description): Competence
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }
}
