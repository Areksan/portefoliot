<?php


namespace ClassList\DataClass;


use ArrayObject;

class Experience
{
    private int $id;
    private string $name;
    private string $nameEntreprise;
    private string|null $start = null;
    private string|null $end = null;
    private string $description;
    private ArrayObject $competences;

    public function __construct()
    {
        $this->competences = new ArrayObject();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @param string $name
     * @return Experience
     */
    public function setName(string $name): Experience
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $nameEntreprise
     * @return Experience
     */
    public function setNameEntreprise(string $nameEntreprise): Experience
    {
        $this->nameEntreprise = $nameEntreprise;
        return $this;
    }

    /**
     * @return string
     */
    public function getNameEntreprise(): string
    {
        return $this->nameEntreprise;
    }

    /**
     * @param string $start
     * @return Experience
     */
    public function setStart(string $start): Experience
    {
        $this->start = $start;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStart(): ?string
    {
        return $this->start;
    }

    /**
     * @param string $end
     * @return Experience
     */
    public function setEnd(string $end): Experience
    {
        $this->end = $end;
        return $this;
    }

    /**
     * @return string | null
     */
    public function getEnd(): ?string
    {
        return $this->end;
    }

    /**
     * @param string $description
     * @return Experience
     */
    public function setDescription(string $description): Experience
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return ArrayObject
     */
    public function getCompetences(): ArrayObject
    {
        return $this->competences;
    }

    /**
     * @param Competence $competence
     * @return Experience
     */
    public function addCompetence(Competence $competence): Experience
    {
        $this->competences->append($competence);
        return $this;
    }

    /**
     * @param Competence $delCompetence
     * @return Experience
     */
    public function deleteCompetence(Competence $delCompetence): Experience
    {
        foreach ($this->competences as $key => $competence) {
            if ($this->competences->offsetGet($key) == $delCompetence) {
                $this->competences->offsetUnset($key);
                break;
            }
        }
        return $this;
    }
}
