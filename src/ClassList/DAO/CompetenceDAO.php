<?php


namespace ClassList\DAO;


use ClassList\DataClass\Competence;
use PDO;

class CompetenceDAO extends InitDAO implements DAOInterface
{
    /**
     * @param object|Competence $data
     * @return array
     */
    function createData(object $data): array
    {
        $prepare = $this->cnx->prepare("INSERT INTO Competence (name, description) VALUES (?,?)");
        $prepare->execute(array(
            $data->getName(),
            $data->getDescription()
        ));
        return $this->getData($this->recuperationLast());
    }

    function getData(int $id = -1): array
    {
        if ($id == -1) {
            $prepare = $this->cnx->prepare("SELECT * FROM Competence");
            $prepare->execute();
        } else {
            $prepare = $this->cnx->prepare("SELECT * FROM Competence WHERE id=?");
            $prepare->execute(array($id));
        }
        return $prepare->fetchAll(PDO::FETCH_CLASS, Competence::class);
    }

    /**
     * @param object|Competence $data
     * @return array
     */
    function updateData(object $data): array
    {
        $prepare = $this->cnx->prepare("UPDATE Competence SET name=?,description=? WHERE id=?");
        $prepare->execute(array(
            $data->getName(),
            $data->getDescription(),
            $data->getId()
        ));
        return $this->getData($data->getId());
    }

    /**
     * @param object|Competence $data
     * @return bool
     */
    function deleteData(object $data): bool
    {
        $prepare = $this->cnx->prepare("DELETE FROM Competence WHERE id=?");
        return $prepare->execute(array($data->getId()));
    }
}
