<?php


namespace ClassList\DAO;


interface DAOInterface
{
    function createData(object $data):array;
    function getData(int $id=-1):array;
    function updateData(object $data):array;
    function deleteData(object $data):bool;
}
