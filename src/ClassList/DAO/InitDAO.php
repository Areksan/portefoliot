<?php


namespace ClassList\DAO;


use ClassList\Service\DbConnexion;
use PDO;

class InitDAO
{
    protected PDO $cnx;
    private array|string $callBy;
    private array $listFunctions;

    public function __construct()
    {
        $this->cnx = DbConnexion::getConnexion()->getCnx();
        $temp = explode('\\', get_called_class());
        $this->callBy = str_replace('DAO', '', end($temp));
        $this->listFunctions = get_class_methods('ClassList\\DataClass\\' . $this->callBy);
    }

    /**
     * @return bool|array|string
     */
    public function getCallBy(): bool|array|string
    {
        return $this->callBy;
    }

    protected function recuperationLast():int
    {
        $prepare = $this->cnx->prepare("SELECT last_insert_rowid()");
        $prepare->execute();
        return intval($prepare->fetchAll()[0]["last_insert_rowid()"]);
    }
}
