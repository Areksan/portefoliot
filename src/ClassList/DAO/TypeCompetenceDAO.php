<?php


namespace ClassList\DAO;


use ClassList\DataClass\TypeCompetence;
use PDO;

class TypeCompetenceDAO extends InitDAO implements DAOInterface
{
    /**
     * @param TypeCompetence $data
     * @return object
     */
    function createData(object $data): array
    {
        $prepare = $this->cnx->prepare("INSERT INTO TypeCompetence (name,description) VALUES (?,?)");
        $prepare->execute(array($data->getName(), $data->getDescription()));
        return $this->getData($this->recuperationLast());
    }

    function getData(int $id = -1): array
    {
        if ($id == -1) {
            $prepare = $this->cnx->prepare("SELECT * FROM TypeCompetence");
            $prepare->execute();
        } else {
            $prepare = $this->cnx->prepare("SELECT * FROM TypeCompetence WHERE id = ? ");
            $prepare->execute(array($id));
        }
        return $prepare->fetchAll(PDO::FETCH_CLASS, TypeCompetence::class);
    }

    /**
     * @param TypeCompetence $data
     * @return array
     */
    function updateData(object $data): array
    {
        $prepare = $this->cnx->prepare("UPDATE TypeCompetence SET name=? , description=? WHERE id = ?");
        $prepare->execute(array($data->getName(), $data->getDescription(), $data->getId()));
        return $this->getData($data->getId());
    }

    /**
     * @param TypeCompetence $data
     * @return bool
     */
    function deleteData(object $data): bool
    {
        $prepare = $this->cnx->prepare("DELETE FROM TypeCompetence WHERE id = ?");
        return $prepare->execute(array($data->getId()));
    }
}
