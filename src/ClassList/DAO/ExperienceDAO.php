<?php


namespace ClassList\DAO;


use ClassList\DataClass\Experience;
use PDO;

class ExperienceDAO extends InitDAO implements DAOInterface
{
    /**
     * @param object|Experience $data
     * @return array
     */
    function createData(object $data): array
    {
        $prepare = $this->cnx->prepare("INSERT INTO Experience (name, nameEntreprise, start, end, description) VALUES (?,?,?,?,?)");
        $prepare->execute(array(
            $data->getName(),
            $data->getNameEntreprise(),
            $data->getStart(),
            $data->getEnd(),
            $data->getDescription()
        ));
        return $this->getData($this->recuperationLast());
    }

    function getData(int $id = -1): array
    {
        if ($id == -1) {
            $prepare = $this->cnx->prepare("SELECT * FROM Experience");
            $prepare->execute();
        } else {
            $prepare = $this->cnx->prepare("SELECT * FROM Experience WHERE id=?");
            $prepare->execute(array($id));
        }
        return $prepare->fetchAll(PDO::FETCH_CLASS, Experience::class);
    }

    /**
     * @param object|Experience $data
     * @return array
     */
    function updateData(object $data): array
    {
        $prepare = $this->cnx->prepare("UPDATE Experience SET name=?,nameEntreprise=?,start=?,end=?,description=? WHERE id=?");
        $prepare->execute(array(
            $data->getName(),
            $data->getNameEntreprise(),
            $data->getStart(),
            $data->getEnd(),
            $data->getDescription(),
            $data->getId()
        ));
        return $this->getData($data->getId());
    }

    /**
     * @param object|Experience $data
     * @return bool
     */
    function deleteData(object $data): bool
    {
        $prepare = $this->cnx->prepare("DELETE FROM Experience WHERE id=?");
        return $prepare->execute(array($data->getId()));
    }
}
